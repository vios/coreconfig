set encoding=utf-8
set shell=/bin/bash

runtime addons.vim
runtime luainit.vim
runtime cmdlets.vim
runtime editor.vim

"Start screen
"
let g:startify_session_persistence = 1
let g:startify_change_to_vcs_root = 1
let g:startify_fortune_use_unicode = 1
let g:startify_padding_left = 4

let g:logo = [
            \ ' _    __    _    ____    _____',
            \ '| |  / /   (_)  / __ \  / ___/',
            \ '| | / /   / /  / / / /  \__ \',
            \ '| |/ /   / /  / /_/ /  ___/ /',
            \ '|___/   /_/   \____/  /____/',
            \ '------------------------------',
            \ '> Preview: 1',
            \ '------------------------------',
            \]

let g:startify_custom_header =
       \ 'map(g:logo + startify#fortune#cowsay(), "\"   \".v:val")'
