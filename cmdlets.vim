""""
" Helper for browsing internal docs
""""

function! AutoNetRwCwd()
    cd %:p:h
endfunction

try
    function! ReloadConfig()
        source $MYVIMRC
    endfunction
catch
endtry

function! TermToggle(height, top)
    if a:top && win_gotoid(g:term_top_win)
        hide
    elseif win_gotoid(g:term_bot_win)
        hide
    else
        if a:top
            top new
        else
            bot new
        endif
        exec "resize " . a:height
        try
            if a:top
                exec "buffer " . g:term_top_buf
            else
                exec "buffer " . g:term_bot_buf
            endif
        catch
            call termopen($SHELL, {"detach":0})
            if a:top
                let g:term_top_buf = bufnr("")
            else
                let g:term_bot_buf = bufnr("")
            endif
            set nonumber
            set norelativenumber
            set signcolumn=no
        endtry
        startinsert!
        if a:top
            let g:term_top_win = win_getid()
        else
            let g:term_bot_win = win_getid()
        endif
    endif
endfunction

function! OpenConfig() abort
    let s:rootdir = expand("$VIMCONFIG")
    let s:file = s:rootdir . "/editor.vim"
    exec "tabedit " . s:file
    exec "lcd " . s:rootdir
endfunction

function! OpenPluginList() abort
    let s:rootdir = expand("$VIMCONFIG")
    let s:file = s:rootdir . "/addons.vim"
    exec "tabedit " . s:file
    exec "lcd " . s:rootdir
endfunction

function! PrevWin(context) abort
    wincmd p
endfunction

function! PrevTab(context) abort
    tabp
endfunction

command! GotoDir RangerCwd
command! Files call fzf#run({'sink': 'edit', 'left': '20%', 'options': '--multi --reverse'})
command! Reload call ReloadConfig()
command! Config call OpenConfig()
command! Plugins call OpenPluginList()
command! Addons call OpenPluginList()
