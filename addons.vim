" Plugin Load File
call plug#begin('~/.local/share/nvim/plugged')

Plug 'Shougo/denite.nvim'
Plug 'myusuf3/numbers.vim'
Plug 'rust-lang/rust.vim'
Plug 'vim-airline/vim-airline' | Plug 'vim-airline/vim-airline-themes'
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
Plug 'mhinz/vim-grepper', { 'on': ['Grepper', '<plug>(GrepperOperator)'] }
Plug 'rbgrouleff/bclose.vim'
Plug 'mhinz/vim-startify'
Plug 'rliang/termedit.nvim'
Plug 'mattn/emmet-vim'
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'sirver/ultisnips' | Plug 'honza/vim-snippets' " Addon for ultisnips
Plug 'othree/yajs.vim'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'othree/html5.vim'
Plug 'vim-pandoc/vim-pandoc' | Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'SirJson/sd.vim'
Plug 'OmniSharp/omnisharp-vim'
Plug 'jiangmiao/auto-pairs'
Plug 'editorconfig/editorconfig-vim'
Plug 'tpope/vim-surround'
Plug 'mbbill/undotree'
Plug 'airblade/vim-gitgutter'
Plug 'ntpeters/vim-better-whitespace'
Plug 'sheerun/vim-polyglot' "More syntax highlighting info!
Plug 'NLKNguyen/papercolor-theme'
Plug 'SirJson/ranger.vim', { 'branch': 'change-directory' }
Plug 'kshenoy/vim-signature'
Plug 'urbainvaes/vim-remembrall'
call plug#end()
