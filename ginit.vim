call rpcnotify(1, 'Gui', 'Font', 'Hack:h10')
call rpcnotify(0, 'Gui', 'Option', 'Tabline', 0)
set termguicolors " truecolor!
colorscheme PaperColor
let g:enable_bold_font = 1
let g:enable_italic_font = 1
let g:airline_theme='papercolor'
autocmd optionset! guicursor
set guicursor=n-v-c-sm:block,i-ci-ve:ver25,r-cr-o:hor20
